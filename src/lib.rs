use ordered_float::OrderedFloat;
use std::{f32::consts::SQRT_2, fmt::Debug, ops::ControlFlow};

use ego_tree::{NodeId, Tree};

pub struct State<G: Game> {
    pub tree: Tree<Node<G>>,
    pub root: NodeId,
    pub turn_at_root: G::Turn,
    pub our_player: G::Turn,
    pub board: G::Board,
}

pub enum MovePolicy {
    RobustChild,
    MaxChild,
    MaxRobustChild,
    Pesimistic,
    SecureChild,
}

impl<G: Game> State<G> {
    pub fn new(turn: G::Turn, player: G::Turn, board: G::Board) -> Self {
        let tree = Tree::new(Node {
            r#move: None,
            playouts: 0,
            score: 0.0,
            is_complete: false,
        });
        Self {
            root: tree.root().id(),
            tree,
            turn_at_root: turn,
            board,
            our_player: player,
        }
    }

    pub fn mcts(&mut self, times: usize) {
        for _ in 0..times {
            if self.mcts_1().is_break() {
                break;
            }
        }
    }

    fn mcts_1(&mut self) -> ControlFlow<()> {
        let Some(selection) = self.select() else {
            return ControlFlow::Break(());
        };
        let simulation = self.simulate(selection);
        self.backpropogate(simulation);

        ControlFlow::Continue(())
    }

    fn select(&self) -> Option<NodeId> {
        let mut node = self.root;
        let (mut board, mut turn) = self.build_board(node);
        loop {
            // self.provide_children(node);
            let node_ref = self.tree.get(node).unwrap();

            if node_ref.value().is_complete {
                return None;
            }

            if node_ref.children().count() < board.legal_moves().len() {
                break;
            }

            let parent_playouts = node_ref.value().playouts;
            let children = node_ref.children();
            let best_child = children
                .filter(|x| !x.value().is_complete)
                .max_by_key(|x| OrderedFloat(x.value().uct(parent_playouts)))
                .unwrap();
            best_child
                .value()
                .r#move
                .as_ref()
                .unwrap()
                .play(&mut board, turn.clone());
            node = best_child.id();
            turn = turn.next();
        }
        Some(node)
    }

    pub fn build_board(&self, id: NodeId) -> (G::Board, G::Turn) {
        let mut board = self.board.clone();
        let mut turn = self.turn_at_root.clone();
        let node = &self.tree.get(id).unwrap();
        let mut ancestors = node.ancestors().collect::<Vec<_>>();
        ancestors.reverse();
        ancestors.push(*node);
        for ancestor in ancestors {
            if ancestor.id() != self.tree.root().id() {
                ancestor
                    .value()
                    .r#move
                    .as_ref()
                    .unwrap()
                    .play(&mut board, turn.clone());
                turn = turn.next();
            }
        }
        (board, turn)
    }

    fn simulate(&mut self, mut node: NodeId) -> NodeId {
        let (mut board, mut turn) = self.build_board(node);
        loop {
            // Get variables
            let node_ref = self.tree.get(node).unwrap();
            let mut moves = board.legal_moves();

            // Get a random move which has not already been created
            moves.retain(|x| {
                !node_ref
                    .children()
                    .any(|a| a.value().r#move.as_ref() == Some(x))
            });
            let mv = chose_random(&moves);

            // Create a new node to represent said move
            mv.play(&mut board, turn.clone());
            let mut node_mut = self.tree.get_mut(node).unwrap();
            let mut new_node = node_mut.append(Node {
                r#move: Some(mv.clone()),
                playouts: 0,
                score: 0.0,
                is_complete: false,
            });

            // Check if the game ends at this move
            let completion_state = board.completion_state();

            if let Some(completion_state) = completion_state {
                // Updates the node's score appropriately
                new_node.value().score = match completion_state {
                    Terminal::Won(completion_state) => {
                        if completion_state.turn == self.our_player {
                            completion_state.score
                        } else {
                            0.0
                        }
                    }
                    Terminal::Drawn => 0.0,
                };

                new_node.value().playouts = 1;
                new_node.value().is_complete = true;

                return new_node.id();
            }

            node = new_node.id();
            turn = turn.next();
        }
    }

    fn backpropogate(&mut self, expansion: NodeId) {
        let node = self.tree.get(expansion).unwrap();
        let score = node.value().score;
        let ancestors = node.ancestors().map(|x| x.id()).collect::<Vec<_>>();

        // Loop through each ancestor and update is information
        for ancestor in ancestors {
            let (board, _turn) = self.build_board(ancestor);
            let ancestor_ref = self.tree.get(ancestor).unwrap();
            let is_complete = ancestor_ref.children().all(|x| x.value().is_complete)
                && ancestor_ref.children().count() == board.legal_moves().len();
            let mut ancestor = self.tree.get_mut(ancestor).unwrap();
            ancestor.value().playouts += 1;
            ancestor.value().score += score;
            ancestor.value().is_complete = is_complete;
        }
    }

    pub fn suggest_move(&self, policy: MovePolicy) -> Option<G::Move> {
        let root = self.tree.get(self.root).unwrap();
        match policy {
            MovePolicy::MaxChild => root
                .children()
                .max_by_key(|x| {
                    (
                        OrderedFloat(
                            dbg!(
                                x.value().score / x.value().playouts as f32,
                                &x.id(),
                                x.value().playouts,
                            )
                            .0,
                        ),
                        x.value().playouts,
                    )
                })
                .and_then(|x| x.value().r#move.clone()),
            MovePolicy::Pesimistic => root
                .children()
                .max_by_key(|x| {
                    x.children()
                        .map(|x| OrderedFloat(x.value().score / x.value().playouts as f32))
                        .min()
                })
                .and_then(|x| x.value().r#move.clone()),
            MovePolicy::RobustChild => root
                .children()
                .max_by_key(|x| {
                    (
                        OrderedFloat({
                            let completion_state = self.build_board(x.id()).0.completion_state();
                            match completion_state {
                                Some(completion_state) => match completion_state {
                                    Terminal::Won(completion_state) => {
                                        if completion_state.turn == self.our_player {
                                            completion_state.score
                                        } else {
                                            0.0
                                        }
                                    }
                                    Terminal::Drawn => 0.0,
                                },
                                None => 0.0,
                            }
                        }),
                        x.value().playouts,
                    )
                })
                .and_then(|x| x.value().r#move.clone()),
            MovePolicy::MaxRobustChild => todo!(),
            MovePolicy::SecureChild => todo!(),
        }
    }

    pub fn play_move(&mut self, r#move: G::Move) {
        let root = self.tree.get(self.root).unwrap();
        let child = root
            .children()
            .find(|x| x.value().r#move.as_ref() == Some(&r#move));
        match child {
            Some(child) => self.root = child.id(),
            None => {
                let mut root = self.tree.get_mut(self.root).unwrap();
                let mut board = self.board.clone();
                r#move.play(&mut board, self.turn_at_root.clone());
                self.root = root
                    .append(Node {
                        r#move: Some(r#move),
                        playouts: 0,
                        score: 0.0,
                        is_complete: board.completion_state().is_some(),
                    })
                    .id();
            }
        }
    }
}

fn chose_random<T>(arr: &[T]) -> &T {
    &arr[fastrand::usize(0..arr.len())]
}

#[derive(Debug)]
pub struct Node<G: Game> {
    // should only be None on root
    pub r#move: Option<G::Move>,
    pub playouts: usize,
    pub score: f32,
    pub is_complete: bool,
}

impl<G: Game> Node<G> {
    pub fn uct(&self, parent_playouts: usize) -> f32 {
        let playouts = self.playouts as f32;
        let score = self.score;
        let parent_playouts = parent_playouts as f32;
        (score / playouts) + SQRT_2 * (parent_playouts.ln() / playouts).sqrt()
    }
}

pub trait Game: Debug {
    type Turn: Turn + Debug;
    type Move: Move<Self::Board, Self::Turn> + Debug;
    type Board: Board<Self::Move, Self::Turn> + Default + Debug;
}

#[derive(Debug)]
pub enum Terminal<T: Turn> {
    Won(WonTerminal<T>),
    Drawn,
}

#[derive(Debug)]
pub struct WonTerminal<T: Turn> {
    pub score: f32,
    pub turn: T,
}

impl<T: Turn> WonTerminal<T> {
    pub fn new(score: f32, turn: T) -> Self {
        Self { score, turn }
    }
}

pub trait Board<M: Move<Self, T>, T: Turn>: Sized + Clone {
    fn legal_moves(&self) -> Vec<M>;
    fn completion_state(&self) -> Option<Terminal<T>>;
}

pub trait Move<B: Board<Self, T>, T: Turn>: Sized + Clone + Eq {
    fn play(&self, board: &mut B, turn: T);
    fn unplay(&self, board: &mut B, turn: T);
}

pub trait Turn: Eq + Clone {
    fn next(self) -> Self;
    fn prev(self) -> Self;
}
