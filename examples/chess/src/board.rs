use std::{
    fmt::{self, Debug},
    mem,
    rc::Rc,
};

use monte_carlo::{Board as _, Move as _, Terminal, Turn};

use crate::{
    game::Player,
    piece::{CastleRole, Piece, PieceTemplate, TemplateRef},
};

#[derive(Clone, Copy, Eq, PartialEq, PartialOrd, Ord, Hash)]
pub struct Cell {
    pub rank: usize,
    pub file: usize,
}

impl Cell {
    pub const FILES: &'static [char] = &[
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
        's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
    ];
}

impl Debug for Cell {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}{}", Self::FILES[self.file], self.rank + 1)
    }
}

#[derive(Clone, PartialEq, Eq, PartialOrd, Ord, Debug, Default)]
pub struct Board {
    /// Our board. `self.board[file][rank]`
    pub board: [[Option<Piece>; 8]; 8],
    /// Pieces which pieces can promote to
    pub promotion_pieces: Vec<TemplateRef>,
    /// All the pieces relevant to this game of chess
    ///
    /// For a standard game of chess, this will be:
    ///
    ///  - king
    ///  - queen
    ///  - rook
    ///  - bishop
    ///  - knight
    ///  - pawn
    pub templates: Rc<Vec<PieceTemplate>>,
    /// Whose turn is it
    pub turn: Player,
    /// The last cell which was moved (useful for en passant)
    pub last_move_end_cell: Option<Cell>,
}

impl Board {
    pub fn contains(&self, cell: Cell) -> bool {
        cell.rank < 8 && cell.file < 8
    }

    pub fn get(&self, cell: Cell) -> &Option<Piece> {
        &self.board[cell.file][cell.rank]
    }

    pub fn get_mut(&mut self, cell: Cell) -> &mut Option<Piece> {
        &mut self.board[cell.file][cell.rank]
    }

    pub fn new() -> Self {
        Self {
            templates: vec![
                PieceTemplate::new_king(),
                PieceTemplate::new_queen(),
                PieceTemplate::new_rook(),
                PieceTemplate::new_bishop(),
                PieceTemplate::new_knight(),
                PieceTemplate::new_pawn(),
            ]
            .into(),
            board: [
                [
                    Some(TemplateRef(2).white()),
                    Some(TemplateRef(5).white()),
                    None,
                    None,
                    None,
                    None,
                    Some(TemplateRef(5).black()),
                    Some(TemplateRef(2).black()),
                ],
                [
                    Some(TemplateRef(4).white()),
                    Some(TemplateRef(5).white()),
                    None,
                    None,
                    None,
                    None,
                    Some(TemplateRef(5).black()),
                    Some(TemplateRef(4).black()),
                ],
                [
                    Some(TemplateRef(3).white()),
                    Some(TemplateRef(5).white()),
                    None,
                    None,
                    None,
                    None,
                    Some(TemplateRef(5).black()),
                    Some(TemplateRef(3).black()),
                ],
                [
                    Some(TemplateRef(1).white()),
                    Some(TemplateRef(5).white()),
                    None,
                    None,
                    None,
                    None,
                    Some(TemplateRef(5).black()),
                    Some(TemplateRef(1).black()),
                ],
                [
                    Some(TemplateRef(0).white()),
                    Some(TemplateRef(5).white()),
                    None,
                    None,
                    None,
                    None,
                    Some(TemplateRef(5).black()),
                    Some(TemplateRef(0).black()),
                ],
                [
                    Some(TemplateRef(3).white()),
                    Some(TemplateRef(5).white()),
                    None,
                    None,
                    None,
                    None,
                    Some(TemplateRef(5).black()),
                    Some(TemplateRef(3).black()),
                ],
                [
                    Some(TemplateRef(4).white()),
                    Some(TemplateRef(5).white()),
                    None,
                    None,
                    None,
                    None,
                    Some(TemplateRef(5).black()),
                    Some(TemplateRef(4).black()),
                ],
                [
                    Some(TemplateRef(2).white()),
                    Some(TemplateRef(5).white()),
                    None,
                    None,
                    None,
                    None,
                    Some(TemplateRef(5).black()),
                    Some(TemplateRef(2).black()),
                ],
            ],
            promotion_pieces: vec![
                TemplateRef(1),
                TemplateRef(2),
                TemplateRef(3),
                TemplateRef(4),
            ],
            turn: Player::White,
            last_move_end_cell: None,
        }
    }

    /// Gets the template from a [`TemplateRef`]
    ///
    /// # Panics
    ///
    /// Panics if `template.0 > self.templates.len()`
    pub fn template(&self, template: TemplateRef) -> &PieceTemplate {
        &self.templates[template.0]
    }

    /// Gets the rooks which can be castled from
    ///
    /// This does exclude rooks which have already moved, but
    /// does not exclude rooks with pieces inbetween them and their respective
    /// king.
    pub fn get_castlable_rooks(&self, player: Player) -> impl Iterator<Item = (Cell, &Piece)> {
        self.iter().filter_map(move |(cell, piece)| match piece {
            Some(piece) => {
                if self.template(piece.template).castling_role == Some(CastleRole::Rooklike)
                    && piece.moves == 0
                    && piece.player == player
                {
                    Some((cell, piece))
                } else {
                    None
                }
            }
            None => None,
        })
    }

    pub fn king_is_in_check(&self, player: Player, legal_moves: &[Move]) -> bool {
        // Find the king
        let (cell, _piece) = self
            .get_king(player)
            .expect("It is not okay to play chess without a king.  Just don't do it.");

        cell_is_in_check(legal_moves, cell)
    }

    /// Gets the player's king
    fn get_king(&self, player: Player) -> Option<(Cell, &Piece)> {
        self.iter().find_map(|(cell, piece)| match piece {
            Some(piece) => {
                if self.template(piece.template).checkable && piece.player == player {
                    Some((cell, piece))
                } else {
                    None
                }
            }
            None => None,
        })
    }

    /// Gets the legal moves, but doesn't worry about if
    /// that move will put the king in check or not.
    pub fn legal_moves_including_check(&self, including_castle: bool) -> Vec<Move> {
        self.legal_moves_including_check_with_turn(self.turn, including_castle)
    }

    /// Gets the legal moves, but doesn't worry about if
    /// that move will put the king in check or not.
    pub fn legal_moves_including_check_with_turn(
        &self,
        turn: Player,
        including_castle: bool,
    ) -> Vec<Move> {
        let mut legal_moves = Vec::new();

        // Go through each piece, and add its
        // legal moves to the Vec
        for (file, file_arr) in self.board.iter().enumerate() {
            for (rank, cell) in file_arr.iter().enumerate() {
                if let Some(cell) = cell {
                    if cell.player == turn {
                        cell.legal_moves(
                            self,
                            Cell { file, rank },
                            &mut legal_moves,
                            including_castle,
                        );
                    }
                }
            }
        }

        legal_moves
    }

    pub fn iter(&self) -> impl Iterator<Item = (Cell, &Option<Piece>)> {
        self.board.iter().enumerate().flat_map(|(file, x)| {
            x.iter()
                .enumerate()
                .map(move |(rank, x)| (Cell { file, rank }, x))
        })
    }

    /// Castles
    ///
    /// # Panics
    ///
    /// Panics if there is not an eligible rook to castle
    /// on desired side.
    ///
    /// # Gotchas
    ///
    /// It will not preform any check regarding the king, so
    /// the caller should ensure that the king is eligible to castle.
    fn castle(&mut self, is_queenside: bool, player: Player) {
        self.last_move_end_cell = None;
        let rooks = self.get_castlable_rooks(player);

        // Get the rook which satisfies the queenside request
        let mut cell = None;
        for (rook_cell, _rook) in rooks {
            let dist_to_a = rook_cell.file;
            let dist_to_h = 8 - rook_cell.file;
            if is_queenside == (dist_to_a < dist_to_h) {
                cell = Some(rook_cell);
            }
        }

        // Move the rook
        Move::Regular {
            start: cell.expect(
                "I regret to inform you that you have asked to
                castle despite the clear lack of rooks eligible
                to castle.  Your visa has hence been denied",
            ),
            end: Cell {
                rank: player.back_rank(),
                file: if is_queenside { 3 } else { 5 },
            },
            promotion: None,
            is_en_passant: false,
        }
        .play(self, player);

        // Move the king
        let (king_cell, _king) = self.get_king(player).expect("Player has no king!");
        Move::Regular {
            start: king_cell,
            end: Cell {
                rank: player.back_rank(),
                file: if is_queenside { 2 } else { 6 },
            },
            promotion: None,
            is_en_passant: false,
        }
        .play(self, player);

        self.turn = self.turn.prev();
    }
}

pub fn cell_is_in_check(legal_moves: &[Move], cell: Cell) -> bool {
    legal_moves.iter().any(|x| match x {
        Move::OO => false,
        Move::OOO => false,
        Move::Regular { end, .. } => {
            if *end == cell {
                true
            } else {
                false
            }
        }
    })
}

/// A move which can be enacted
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone)]
pub enum Move {
    /// Kingside castling
    OO,
    /// Queenside castling
    OOO,
    /// A move of a piece from start to destination
    Regular {
        start: Cell,
        end: Cell,
        promotion: Option<TemplateRef>,
        is_en_passant: bool,
    },
}

impl monte_carlo::Move<Board, Player> for Move {
    fn play(&self, board: &mut Board, turn: Player) {
        match self {
            Move::OO => board.castle(false, turn),
            Move::OOO => board.castle(true, turn),
            Move::Regular {
                start,
                end,
                promotion,
                is_en_passant,
            } => {
                let piece = mem::take(&mut board.board[start.file][start.rank]);
                let moves = piece.as_ref().map(|x| x.moves).unwrap_or_default() + 1;
                let player = piece.as_ref().map(|x| x.player).unwrap();
                let piece_template = promotion.clone().or(piece.map(|x| x.template));

                // Remove the en passant'd pawn, if applicable
                if *is_en_passant {
                    let en_passant_square = Cell {
                        file: end.file,
                        rank: end
                            .rank
                            .checked_add_signed(-player.forward_direction())
                            .unwrap(),
                    };
                    *board.get_mut(en_passant_square) = None;
                }

                board.last_move_end_cell = Some(*end);

                *board.get_mut(*end) = piece_template.map(|template| Piece {
                    template,
                    moves,
                    player,
                });
                board.turn = board.turn.next();
            }
        }
    }
    fn unplay(&self, board: &mut Board, turn: Player) {
        unreachable!();
    }
}

impl monte_carlo::Board<Move, Player> for Board {
    fn legal_moves(&self) -> Vec<Move> {
        let mut moves = self.legal_moves_including_check(false);
        filter_legal_moves_for_check(&mut moves, self);
        moves
    }

    fn completion_state(&self) -> Option<Terminal<Player>> {
        let mvs = self.legal_moves();
        let mut x = self.clone();
        x.turn = x.turn.next();
        let their_moves = x.legal_moves();

        // TODO: Threefold repetition, fifty-move rule.
        if mvs.is_empty() && self.king_is_in_check(self.turn, &their_moves) {
            Some(Terminal::Won(monte_carlo::WonTerminal {
                turn: self.turn.prev(),
                score: 1.0,
            }))
        } else if mvs.is_empty() {
            Some(Terminal::Drawn)
        } else {
            None
        }
    }
}

pub fn filter_legal_moves_for_check(legal_moves: &mut Vec<Move>, board: &Board) {
    legal_moves.retain(|mv| {
        let mut board = board.clone();
        let our_turn = board.turn;
        mv.play(&mut board, our_turn);
        let moves = board.legal_moves_including_check(false);
        !board.king_is_in_check(our_turn, &moves)
    });
}
