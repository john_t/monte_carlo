use ui::GameState;

mod board;
mod game;
mod piece;
mod ui;

fn main() -> anyhow::Result<()> {
    GameState::new()?.run()
}
