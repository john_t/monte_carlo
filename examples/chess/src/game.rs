use monte_carlo::Game;

use crate::board::{Board, Move};

#[derive(Debug)]
pub struct Chess;

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Default)]
pub enum Player {
    #[default]
    White,
    Black,
}

impl Player {
    pub const fn promotion_rank(&self) -> usize {
        match self {
            Player::White => 7,
            Player::Black => 0,
        }
    }

    pub const fn back_rank(&self) -> usize {
        7 - self.promotion_rank()
    }

    pub const fn forward_direction(&self) -> isize {
        match self {
            Player::White => 1,
            Player::Black => -1,
        }
    }
}

impl monte_carlo::Turn for Player {
    fn next(self) -> Self {
        match self {
            Self::White => Self::Black,
            Self::Black => Self::White,
        }
    }

    fn prev(self) -> Self {
        self.next()
    }
}

impl Game for Chess {
    type Turn = Player;
    type Move = Move;
    type Board = Board;
}
