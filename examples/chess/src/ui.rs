use std::{borrow::Cow, ops::ControlFlow};

use crossterm::event::{Event, KeyCode};
use either::Either;
use monte_carlo::{Board as _, Move as _, MovePolicy, State, Terminal};
use tuviv::{
    le::{grid::Sizing, layout::TRBL, Alignment, Orientation},
    prelude::{StylableExt, WidgetExt},
    widgets::{
        grid::{self, Props},
        Char, Filler, Flexbox, Grid, Paragraph,
    },
    App, Color, Style, Widget,
};

use crate::{
    board::{filter_legal_moves_for_check, Board, Cell, Move},
    game::{Chess, Player},
};

/// The state of a ui enabled game of chess
pub struct GameState {
    pub board: Board,
    pub app: App,
    pub cursor: Cell,
    pub legal_moves: Vec<Move>,
    pub selection: Option<Cell>,
}

impl GameState {
    pub fn new() -> anyhow::Result<Self> {
        Ok(Self {
            board: Board::new(),
            app: App::new()?,
            cursor: Cell { rank: 0, file: 0 },
            legal_moves: Vec::new(),
            selection: None,
        })
    }

    pub fn run(&mut self) -> anyhow::Result<()> {
        //let mut state = State::<Chess>::new(Player::White, Player::White, Board::new());
        //state.mcts(1);
        //state.play_move(state.suggest_move(MovePolicy::RobustChild).unwrap());
        //self.board = state.build_board(state.root).0;
        loop {
            let event = crossterm::event::read()?;
            if let ControlFlow::Break(_) = self.handle_input(event) {
                break;
            }

            let to_highlight = self.get_highlights();

            let widget = self.widget(&to_highlight);
            self.app.render_widget(widget)?;
        }

        Ok(())
    }

    fn get_highlights(&mut self) -> Vec<Cell> {
        // Get the legal moves to highlight
        let legal_cursor = self.selection.unwrap_or(self.cursor);
        self.legal_moves = match self.board.get(legal_cursor) {
            Some(piece) => {
                let mut legal_moves = Vec::new();
                if piece.player == self.board.turn {
                    piece.legal_moves(&self.board, legal_cursor, &mut legal_moves, true);
                    filter_legal_moves_for_check(&mut legal_moves, &self.board);
                }
                legal_moves
            }
            None => vec![],
        };
        let to_highlight: Vec<_> = self
            .legal_moves
            .iter()
            .map(|x| match x {
                Move::OO => Cell {
                    rank: self.board.turn.back_rank(),
                    file: 6,
                },
                Move::OOO => Cell {
                    rank: self.board.turn.back_rank(),
                    file: 2,
                },
                Move::Regular { end, .. } => *end,
            })
            .collect();
        to_highlight
    }

    fn handle_input(&mut self, event: Event) -> ControlFlow<()> {
        match event {
            Event::Key(event) => match event.code {
                KeyCode::Char('q') => return ControlFlow::Break(()),
                KeyCode::Left => self.cursor.file = self.cursor.file.saturating_sub(1),
                KeyCode::Right => self.cursor.file = (self.cursor.file + 1).min(7),
                KeyCode::Up => self.cursor.rank = (self.cursor.rank + 1).min(7),
                KeyCode::Down => self.cursor.rank = self.cursor.rank.saturating_sub(1),
                KeyCode::Enter => self.try_move(),
                KeyCode::Char('O') => self.try_castle(true),
                KeyCode::Char('o') => self.try_castle(false),
                _ => (),
            },
            _ => (),
        }
        ControlFlow::Continue(())
    }

    fn try_move(&mut self) {
        match self.selection {
            Some(selection_v) => {
                self.selection = None;
                let piece = self.board.get(selection_v);
                if let Some(piece) = piece {
                    if piece.player == self.board.turn {
                        let mv = self.legal_moves.iter().find(|x| match x {
                            Move::OO => false,
                            Move::OOO => false,
                            Move::Regular { start, end, .. } => {
                                start == &selection_v && end == &self.cursor
                            }
                        });
                        if let Some(mv) = mv {
                            let turn = self.board.turn;
                            mv.play(&mut self.board, turn);
                        }
                    }
                }
            }
            None => self.selection = Some(self.cursor),
        }
    }

    fn widget(&self, to_highlight: &[Cell]) -> impl Widget {
        Flexbox::new(Orientation::Horizontal, false)
            .child(self.widget_board(to_highlight).to_flex_child())
            .child(self.widget_info().to_flex_child())
    }

    fn widget_info(&self) -> impl Widget {
        Flexbox::new(Orientation::Vertical, false)
            .child(self.widget_to_play().to_flex_child())
            .child(self.widget_completion().to_flex_child())
            .to_box_sizing()
            .padding(TRBL::new(4, 4, 4, 4))
    }

    fn widget_to_play(&self) -> impl Widget {
        let text = match self.board.turn {
            Player::White => "\u{f4aa}  to play",
            Player::Black => "\u{f111}  to play",
        };

        Paragraph::label(text.styled())
    }

    fn widget_completion(&self) -> impl Widget {
        let text = match self.board.completion_state() {
            Some(terminal) => match terminal {
                Terminal::Won(won) => match won.turn {
                    Player::White => "\u{f4aa}  wins",
                    Player::Black => "\u{f111}  wins",
                },
                Terminal::Drawn => "draw",
            },
            None => "",
        };
        Paragraph::label(text.styled())
    }

    fn widget_board(&self, to_highlight: &[Cell]) -> impl Widget {
        let mut grid = Grid::new()
            .template_columns(vec![Sizing::Fixed(2); 10])
            .template_rows(vec![Sizing::Fixed(1); 10]);

        // Add file labels
        for (file_i, _file) in self.board.board.iter().enumerate() {
            let name = Cell::FILES[file_i];
            let par = Paragraph::label(name.to_string().styled().dimmed());
            grid.children.push(grid::Child {
                widget: Box::new(par.clone()),
                props: Props {
                    column: file_i + 1,
                    row: 0,
                    column_span: 1,
                    row_span: 1,
                },
            });

            grid.children.push(grid::Child {
                widget: Box::new(par),
                props: Props {
                    column: file_i + 1,
                    row: 9,
                    column_span: 1,
                    row_span: 1,
                },
            });
        }

        // Add rank labels
        for (rank_i, _rank) in self.board.board.iter().enumerate() {
            let name = rank_i + 1;
            let par = Paragraph::label(name.to_string().styled().dimmed());
            grid.children.push(grid::Child {
                widget: Box::new(par.clone()),
                props: Props {
                    column: 0,
                    row: 8 - rank_i,
                    column_span: 1,
                    row_span: 1,
                },
            });

            grid.children.push(grid::Child {
                widget: Box::new(par.align_x(Alignment::End)),
                props: Props {
                    column: 9,
                    row: 8 - rank_i,
                    column_span: 1,
                    row_span: 1,
                },
            });
        }

        for (file_i, file) in self.board.board.iter().enumerate() {
            for (rank_i, rank) in file.iter().enumerate() {
                let piece = rank;
                let cell = Cell {
                    file: file_i,
                    rank: rank_i,
                };
                let selected = cell == self.cursor;
                let in_legal_moves = to_highlight.contains(&cell);

                // Get the background colour
                let bg_color = match (
                    (file_i * 7 + rank_i) % 2 == 0,
                    in_legal_moves,
                    self.selection == Some(cell),
                ) {
                    (_, _, true) => Color::Yellow,
                    (_, true, _) => Color::BrightYellow,
                    (true, false, _) => Color::BrightGreen,
                    (false, false, _) => Color::White,
                };

                let widget: Box<dyn Widget> = if let Some(ref piece) = piece {
                    let template = self.board.template(piece.template);
                    // Get the icon for the piece
                    let c: Cow<_> = match (piece.player, template.algabraic_name) {
                        (Player::Black, 'p') => "♟ ".into(),
                        (Player::Black, 'N') => "♞ ".into(),
                        (Player::Black, 'B') => "♝ ".into(),
                        (Player::Black, 'R') => "♜ ".into(),
                        (Player::Black, 'Q') => "♛ ".into(),
                        (Player::Black, 'K') => "♚ ".into(),
                        (Player::White, 'p') => "♙ ".into(),
                        (Player::White, 'N') => "♘ ".into(),
                        (Player::White, 'B') => "♗ ".into(),
                        (Player::White, 'R') => "♖ ".into(),
                        (Player::White, 'Q') => "♕ ".into(),
                        (Player::White, 'K') => "♔ ".into(),
                        (_, x) => format!("{} ", x).into(),
                    };

                    // Get the style of the piece
                    let mut style = Style::default().fg(Color::Black).bg(bg_color);

                    if selected {
                        style = style.reversed();
                    }

                    Box::new(Paragraph::label(c.with_style(style)).stacked_background(bg_color))
                } else {
                    Box::new(Filler::new(if selected {
                        " ".styled().bg(bg_color).reversed()
                    } else {
                        " ".styled().bg(bg_color)
                    }))
                };

                grid.children.push(grid::Child {
                    widget,
                    props: grid::Props {
                        column: file_i + 1,
                        row: 8 - rank_i,
                        column_span: 1,
                        row_span: 1,
                    },
                })
            }
        }

        grid
    }

    fn try_castle(&mut self, is_queenside: bool) {
        let turn = self.board.turn;
        if is_queenside && self.legal_moves.contains(&Move::OOO) {
            Move::OOO.play(&mut self.board, turn);
        } else if !is_queenside && self.legal_moves.contains(&Move::OO) {
            Move::OO.play(&mut self.board, turn);
        }
    }
}
