use std::num::NonZeroU8;

use bitflags::bitflags;
use monte_carlo::Turn as _;

use crate::{
    board::{cell_is_in_check, Board, Cell, Move},
    game::Player,
};

/// The direction in which a piece can move
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone)]
pub enum Direction {
    Forwards,
    Backwards,
    Horizontal,
    DiagonalForwards,
    DiagonalBackwards,
    Hippogonal { a: u8, b: u8 },
}

/// A move
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone)]
pub struct MoveTemplate {
    /// The direction of the move
    pub direction: Direction,
    /// How many times the move occurs
    pub times: Option<NonZeroU8>,
    /// When said move can occur
    pub condition: Condition,
}

impl MoveTemplate {
    pub const fn new(direction: Direction, times: u8, condition: Condition) -> Self {
        Self {
            direction,
            times: NonZeroU8::new(times),
            condition,
        }
    }

    /// Gets the relative directions in which this can move.
    pub fn dirs(&self, player: &Player) -> Box<[(isize, isize)]> {
        match &self.direction {
            Direction::Forwards => Box::new([(0, player.forward_direction())]),
            Direction::Backwards => Box::new([(0, -player.forward_direction())]),
            Direction::Horizontal => Box::new([(1, 0), (-1, 0)]),
            Direction::DiagonalForwards => Box::new([
                (1, player.forward_direction()),
                (-1, player.forward_direction()),
            ]),
            Direction::DiagonalBackwards => Box::new([
                (1, -player.forward_direction()),
                (-1, -player.forward_direction()),
            ]),

            Direction::Hippogonal { a, b } => {
                let a = *a as isize;
                let b = *b as isize;
                Box::new([
                    (a, b),
                    (a, -b),
                    (-a, b),
                    (-a, -b),
                    (b, a),
                    (-b, a),
                    (b, -a),
                    (-b, -a),
                ])
            }
        }
    }
}

bitflags! {
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone)]
    pub struct Condition: u8 {
        /// The move can happen on the initial move
        const INITIAL_MOVE = 0b00000001;
        /// The move can occur on a (non initial) move
        const FOLLOWING_MOVES = 0b00000010;
        /// The move can occur on a capture
        const CAPTURE = 0b00000100;
        /// The move can occur on a (non capture) move
        const MOVE = 0b00001000;
    }
}

/// Something involved in castling
#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone)]
pub enum CastleRole {
    /// A king like thing
    Kinglike,
    /// A rook like thing
    Rooklike,
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Default)]
pub struct PieceTemplate {
    /// The name for this piece in algabraic notation
    pub algabraic_name: char,
    /// The valid moves for this piece
    pub moves: Vec<MoveTemplate>,
    /// Whether this move is 'checkable'
    pub checkable: bool,
    /// Whether this is a castling source (king) or sink (rook)
    pub castling_role: Option<CastleRole>,
    /// Whether the piece can promote by reaching the final rank
    ///
    /// TODO: Make the rank optional
    pub promotable: bool,
    /// Can engage in en passant?
    pub en_passantable: bool,
}

impl PieceTemplate {
    pub fn new_king() -> Self {
        Self {
            algabraic_name: 'K',
            moves: vec![
                MoveTemplate::new(Direction::Backwards, 1, Condition::all()),
                MoveTemplate::new(Direction::Forwards, 1, Condition::all()),
                MoveTemplate::new(Direction::DiagonalBackwards, 1, Condition::all()),
                MoveTemplate::new(Direction::DiagonalForwards, 1, Condition::all()),
                MoveTemplate::new(Direction::Horizontal, 1, Condition::all()),
            ],
            checkable: true,
            castling_role: Some(CastleRole::Kinglike),
            promotable: false,
            en_passantable: false,
        }
    }

    pub fn new_queen() -> Self {
        Self {
            algabraic_name: 'Q',
            moves: vec![
                MoveTemplate::new(Direction::Backwards, 0, Condition::all()),
                MoveTemplate::new(Direction::Forwards, 0, Condition::all()),
                MoveTemplate::new(Direction::DiagonalBackwards, 0, Condition::all()),
                MoveTemplate::new(Direction::DiagonalForwards, 0, Condition::all()),
                MoveTemplate::new(Direction::Horizontal, 0, Condition::all()),
            ],
            ..Default::default()
        }
    }

    pub fn new_bishop() -> Self {
        Self {
            algabraic_name: 'B',
            moves: vec![
                MoveTemplate::new(Direction::DiagonalBackwards, 0, Condition::all()),
                MoveTemplate::new(Direction::DiagonalForwards, 0, Condition::all()),
            ],
            ..Default::default()
        }
    }

    pub fn new_rook() -> Self {
        Self {
            algabraic_name: 'R',
            moves: vec![
                MoveTemplate::new(Direction::Backwards, 0, Condition::all()),
                MoveTemplate::new(Direction::Forwards, 0, Condition::all()),
                MoveTemplate::new(Direction::Horizontal, 0, Condition::all()),
            ],
            castling_role: Some(CastleRole::Rooklike),
            ..Default::default()
        }
    }

    pub fn new_pawn() -> Self {
        Self {
            algabraic_name: 'p',
            moves: vec![
                MoveTemplate::new(Direction::Forwards, 1, !Condition::CAPTURE),
                MoveTemplate::new(
                    Direction::Forwards,
                    2,
                    Condition::INITIAL_MOVE | Condition::MOVE,
                ),
                MoveTemplate::new(Direction::DiagonalForwards, 1, !Condition::MOVE),
            ],
            en_passantable: true,
            ..Default::default()
        }
    }

    pub fn new_knight() -> Self {
        Self {
            algabraic_name: 'N',
            moves: vec![MoveTemplate::new(
                Direction::Hippogonal { a: 1, b: 2 },
                1,
                Condition::all(),
            )],
            ..Default::default()
        }
    }
}

/// A reference to a piece in a board `templates` field
#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Debug, Default)]
pub struct TemplateRef(pub usize);

impl TemplateRef {
    /// Creates a new white piece from this
    pub fn white(self) -> Piece {
        self.from_player(Player::White)
    }

    /// Creates a new black piece from this
    pub fn black(self) -> Piece {
        self.from_player(Player::Black)
    }

    fn from_player(self, player: Player) -> Piece {
        Piece {
            template: self,
            moves: 0,
            player,
        }
    }
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone)]
pub struct Piece {
    /// The template upon which this piece is based
    pub template: TemplateRef,
    /// The amount of times this piece has moved
    pub moves: usize,
    /// Our colour
    pub player: Player,
}

impl Piece {
    pub fn legal_moves(
        &self,
        board: &Board,
        our_position: Cell,
        legal_moves: &mut Vec<Move>,
        include_castling: bool,
    ) {
        let template = board.template(self.template);

        // Check if we can castle
        if include_castling
            && template.castling_role == Some(CastleRole::Kinglike)
            && self.moves == 0
        {
            eprintln!("Including castling");
            // Get the legal moves, not including castling, so
            // that we can determine whether a square is currently
            // 'in check' or not.
            let current_legal_moves =
                board.legal_moves_including_check_with_turn(board.turn, false);

            // Find all the rooks we may possibly be able to castle with
            let rooks = board.get_castlable_rooks(self.player);

            // See if we can castle
            'rook: for (rook_cell, _rook) in rooks {
                // Determine if this is a queenside rook
                let dist_to_a = rook_cell.file;
                let dist_to_h = 8 - rook_cell.file;
                let is_queenside = dist_to_a < dist_to_h;

                let (king_end_file, rook_end_file) = if is_queenside { (2, 3) } else { (6, 5) };

                // Check if there are any pieces between the king and its destination
                // Also checks if that there is no check
                for file in range_smallest_first_inclusive(king_end_file, our_position.file) {
                    let cell = Cell {
                        rank: self.player.back_rank(),
                        file,
                    };

                    if !cell_is_in_check(&current_legal_moves, cell)
                        && (board.get(cell).is_some()
                            && !(cell == rook_cell || cell == our_position))
                    {
                        eprintln!("Cannot castle cause of king");
                        continue 'rook;
                    }
                }

                // Check if there are any pieces between the rook and its destination
                for file in range_smallest_first_inclusive(rook_end_file, rook_cell.file) {
                    let cell = Cell {
                        rank: self.player.back_rank(),
                        file,
                    };

                    if board.get(cell).is_some() && !(cell == rook_cell || cell == our_position) {
                        eprintln!("Cannot castle cause of rook");
                        continue 'rook;
                    }
                }

                if is_queenside {
                    legal_moves.push(Move::OOO);
                } else {
                    legal_moves.push(Move::OO);
                }
            }
        }

        for mv in &template.moves {
            // Check if this move is legal to be played under the current
            // condition of the piece
            if (self.moves >= 1 && !mv.condition.contains(Condition::FOLLOWING_MOVES))
                || (self.moves == 0 && !mv.condition.contains(Condition::INITIAL_MOVE))
            {
                continue;
            }

            let dirs = mv.dirs(&self.player);

            for dir in &*dirs {
                // TODO: Allow non-8 boards
                'times: for times in 1..=mv.times.map(NonZeroU8::get).unwrap_or(8) {
                    // Get the position this move refers for
                    let pos = (
                        our_position.file as isize + dir.0 * isize::from(times),
                        our_position.rank as isize + dir.1 * isize::from(times),
                    );

                    // Check this is on the board
                    if pos.0 < 0 || pos.1 < 0 || pos.0 >= 8 || pos.1 >= 8 {
                        continue;
                    }
                    let pos = Cell {
                        file: pos.0 as usize,
                        rank: pos.1 as usize,
                    };

                    // Gets the piece from the board
                    let piece = board.get(pos);

                    // Determine if we accept this move, and if
                    // we do is it en passant?
                    let (acceptable, is_en_passant) = match piece {
                        Some(piece) => (
                            piece.player != self.player
                                && mv.condition.contains(Condition::CAPTURE),
                            false,
                        ),
                        None => {
                            let en_passant_rank = pos
                                .rank
                                .checked_add_signed(-self.player.forward_direction());
                            let can_en_passant = if let Some(en_passant_rank) = en_passant_rank {
                                // Get the cell which we would en passant
                                // if we can
                                let en_passant_cell = Cell {
                                    file: pos.file,
                                    rank: en_passant_rank,
                                };
                                if board.contains(en_passant_cell) {
                                    let en_passant_cell_value = board.get(en_passant_cell).as_ref();
                                    board.last_move_end_cell == Some(en_passant_cell)
                                        && template.en_passantable
                                        && en_passant_cell_value
                                            .map(|x| {
                                                x.player != self.player
                                                    && board.template(x.template).en_passantable
                                            })
                                            .unwrap_or(false)
                                        && mv.condition.contains(Condition::CAPTURE)
                                } else {
                                    false
                                }
                            } else {
                                false
                            };

                            if mv.condition.contains(Condition::MOVE) {
                                (true, false)
                            } else if can_en_passant {
                                (true, true)
                            } else {
                                (false, false)
                            }
                        }
                    };

                    // Add the legal move if it was acceptable
                    if acceptable {
                        // Get any promotions.
                        let promotion =
                            if template.promotable && pos.rank == self.player.promotion_rank() {
                                // TODO: Allow underpomoting, and
                                // alternative promotions
                                Some(TemplateRef(1))
                            } else {
                                None
                            };

                        // Append this legal move
                        legal_moves.push(Move::Regular {
                            start: our_position,
                            end: pos,
                            // TODO: Allow underpomoting, and
                            // alternative promotions
                            promotion,
                            is_en_passant,
                        });

                        if piece.is_some() {
                            break 'times;
                        }
                    } else {
                        break 'times;
                    }
                }
            }
        }
    }
}

/// Creates a range iterator starting with the smaller of the two numbers (inclusive)
fn range_smallest_first_inclusive(a: usize, b: usize) -> impl Iterator<Item = usize> {
    let min = a.min(b);
    let max = a.max(b);
    min..=max
}
