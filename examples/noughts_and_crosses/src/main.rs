use graphvis_ego_tree::TreeWrapper;
use monte_carlo::{Board as _, MovePolicy, State, WonTerminal};
use std::io::Write;
use std::ops::ControlFlow;
use std::{
    fmt::{self, Display},
    fs::File,
};

pub const MASKS: &[[usize; 3]] = &[
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],
    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],
    [0, 4, 8],
    [2, 4, 6],
];

pub const MOVE_POLICY: MovePolicy = MovePolicy::MaxChild;

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum Player {
    Cross,
    Nought,
}

impl Display for Player {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Player::Cross => 'X',
                Player::Nought => 'O',
            }
        )
    }
}

impl monte_carlo::Turn for Player {
    fn next(self) -> Self {
        match self {
            Player::Cross => Self::Nought,
            Player::Nought => Self::Cross,
        }
    }

    fn prev(self) -> Self {
        self.next()
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub struct Move(u8);

impl monte_carlo::Move<Board, Player> for Move {
    fn play(&self, board: &mut Board, turn: Player) {
        board.board[usize::from(self.0)] = Some(turn);
    }

    fn unplay(&self, board: &mut Board, _turn: Player) {
        board.board[usize::from(self.0)] = None;
    }
}

#[derive(Default, Clone, Copy, Debug)]
pub struct Board {
    pub board: [Option<Player>; 9],
}

impl Display for Board {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fn cell(player: Option<Player>) -> char {
            match player {
                Some(player) => match player {
                    Player::Cross => 'X',
                    Player::Nought => 'O',
                },
                None => '·',
            }
        }
        write!(
            f,
            "{}{}{}\n{}{}{}\n{}{}{}",
            cell(self.board[0]),
            cell(self.board[1]),
            cell(self.board[2]),
            cell(self.board[3]),
            cell(self.board[4]),
            cell(self.board[5]),
            cell(self.board[6]),
            cell(self.board[7]),
            cell(self.board[8]),
        )
    }
}

type Terminal = monte_carlo::Terminal<Player>;

impl monte_carlo::Board<Move, Player> for Board {
    fn legal_moves(&self) -> Vec<Move> {
        if self.completion_state().is_some() {
            vec![]
        } else {
            let mut v = Vec::with_capacity(9);
            for (i, x) in self.board.iter().enumerate() {
                if x.is_none() {
                    v.push(Move(i as u8))
                }
            }
            v
        }
    }

    fn completion_state(&self) -> Option<Terminal> {
        for mask in MASKS {
            let x0 = self.board[mask[0]];
            let x1 = self.board[mask[1]];
            let x2 = self.board[mask[2]];
            if let (Some(x0), Some(x1), Some(x2)) = (x0, x1, x2) {
                if x0 == x1 && x0 == x2 {
                    return Some(Terminal::Won(WonTerminal::new(1.0, x0)));
                }
            }
        }
        if self.board.iter().all(|x| x.is_some()) {
            Some(Terminal::Drawn)
        } else {
            None
        }
    }
}

#[derive(Debug)]
pub struct NoughtsAndCrosses;

impl monte_carlo::Game for NoughtsAndCrosses {
    type Move = Move;
    type Board = Board;
    type Turn = Player;
}

fn main() {
    let mut state =
        State::<NoughtsAndCrosses>::new(Player::Cross, Player::Cross, Default::default());
    loop {
        state.mcts(6000);
        write_tree_to_dot(&state);
        let mv = state.suggest_move(MOVE_POLICY);
        match mv {
            Some(mv) => {
                state.play_move(mv);
            }
            None => panic!("I don't know how to play!"),
        }
        let root = state.root;
        let (board, _) = state.build_board(root);
        println!("--- Them");
        println!("{}", board);
        let mv: u8 = uquery::parsable("Enter move (0-8): ").unwrap();
        state.play_move(Move(mv));
        let root = state.root;
        let (board, _) = state.build_board(root);
        println!("--- You");
        println!("{}", board);
    }
}

fn write_tree_to_dot(state: &State<NoughtsAndCrosses>) {
    let mut file = File::create("file.dot").unwrap();
    writeln!(
        file,
        "{}",
        TreeWrapper::new(&state.tree, state.root, |x| format!(
            "{}\nid: {:?}\nmove: {:?}\nplayouts: {}\nscore: {}\nis_complete: {}",
            state.build_board(x.id()).0,
            x.id(),
            x.value().r#move,
            x.value().playouts,
            x.value().score,
            x.value().is_complete,
        ))
    )
    .unwrap();
}

/*fn main() {
    let mut state1 =
        State::<NoughtsAndCrosses>::new(Player::Cross, Player::Cross, Board::default());
    let mut state2 =
        State::<NoughtsAndCrosses>::new(Player::Cross, Player::Nought, Board::default());

    loop {
        if play(&mut state1, &mut state2).is_break() {
            break;
        }
        //let mut file = File::create("file.dot").unwrap();
        //writeln!(
        //    file,
        //    "{}",
        //    TreeWrapper::new(&state2.tree, state2.root, |x| format!(
        //        "id: {:?}\nmove: {:?}\nplayouts: {}\nscore: {}\nis_complete: {}",
        //        x.id(),
        //        x.value().r#move,
        //        x.value().playouts,
        //        x.value().score,
        //        x.value().is_complete,
        //    ))
        //)
        //.unwrap();
        if play(&mut state2, &mut state1).is_break() {
            break;
        }
    }
}*/

fn play(
    us: &mut State<NoughtsAndCrosses>,
    them: &mut State<NoughtsAndCrosses>,
) -> ControlFlow<(), ()> {
    us.mcts(12000);
    them.mcts(12000);
    dbg!(us.tree.get(us.root).unwrap().value());
    let mv = us.suggest_move(MOVE_POLICY).unwrap();
    us.play_move(mv);
    them.play_move(mv);

    let root = us.root;
    let (board, turn) = us.build_board(root);
    println!("--- {}", us.our_player);
    println!("{}", board);

    if let Some(terminal) = board.completion_state() {
        match terminal {
            Terminal::Won(won) => println!("Player {} won!", won.turn),
            Terminal::Drawn => println!("Draw"),
        }
        ControlFlow::Break(())
    } else {
        ControlFlow::Continue(())
    }
}
