use core::fmt;
use std::{
    fmt::{Display, Write as _},
    fs::File,
    io::Write as _,
};

use graphvis_ego_tree::TreeWrapper;
use monte_carlo::{Board as _, MovePolicy, State, WonTerminal};
pub const MOVE_POLICY: MovePolicy = MovePolicy::RobustChild;

#[derive(Debug)]
pub struct FourInARow;

#[derive(Default, Clone, Copy, Debug)]
pub struct Board {
    pub board: [[Option<Player>; 6]; 7],
}

type Terminal = monte_carlo::Terminal<Player>;

impl monte_carlo::Board<Move, Player> for Board {
    fn legal_moves(&self) -> Vec<Move> {
        if self.completion_state().is_some() {
            vec![]
        } else {
            let mut v = Vec::with_capacity(7);
            for (i, row) in self.board.iter().enumerate() {
                if row[5].is_none() {
                    v.push(Move(i as u8));
                }
            }
            v
        }
    }

    fn completion_state(&self) -> Option<Terminal> {
        for x in 0..7 {
            for y in 0..6 {
                let o = self.board[x][y];
                let h1 = self.board.get(x + 1).and_then(|p| p.get(y));
                let h2 = self.board.get(x + 2).and_then(|p| p.get(y));
                let h3 = self.board.get(x + 3).and_then(|p| p.get(y));
                let v1 = self.board.get(x).and_then(|p| p.get(y + 1));
                let v2 = self.board.get(x).and_then(|p| p.get(y + 2));
                let v3 = self.board.get(x).and_then(|p| p.get(y + 3));
                let d1 = self.board.get(x + 1).and_then(|p| p.get(y + 1));
                let d2 = self.board.get(x + 2).and_then(|p| p.get(y + 2));
                let d3 = self.board.get(x + 3).and_then(|p| p.get(y + 3));
                let a1 = self
                    .board
                    .get(x + 1)
                    .and_then(|p| p.get(y.saturating_sub(1)));
                let a2 = self
                    .board
                    .get(x + 2)
                    .and_then(|p| p.get(y.saturating_sub(2)));
                let a3 = self
                    .board
                    .get(x + 3)
                    .and_then(|p| p.get(y.saturating_sub(3)));

                let won = o.is_some()
                    && ((Some(&o) == h1 && h1 == h2 && h2 == h3)
                        || (Some(&o) == d1 && d1 == d2 && d2 == d3)
                        || (Some(&o) == a1 && a1 == a2 && a2 == a3 && y >= 3)
                        || (Some(&o) == v1 && v1 == v2 && v2 == v3));
                if won {
                    return Some(Terminal::Won(WonTerminal {
                        turn: o.unwrap(),
                        score: 1.0,
                    }));
                }
            }
        }
        if self.board.iter().all(|x| x.iter().all(|y| y.is_some())) {
            Some(Terminal::Drawn)
        } else {
            None
        }
    }
}

impl Display for Board {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "0 1 2 3 4 5 6")?;
        for y in 0..6 {
            let y = 5 - y;
            writeln!(
                f,
                "{}{}{}{}{}{}{}",
                cell(self.board[0][y]),
                cell(self.board[1][y]),
                cell(self.board[2][y]),
                cell(self.board[3][y]),
                cell(self.board[4][y]),
                cell(self.board[5][y]),
                cell(self.board[6][y]),
            )?
        }
        writeln!(f, " 0 1 2 3 4 5 6")
    }
}

impl Board {
    pub fn fmt_no_color(&self) -> Result<String, fmt::Error> {
        let mut f = String::new();
        writeln!(f, " 0 1 2 3 4 5 6")?;
        for y in 0..6 {
            let y = 5 - y;
            writeln!(
                f,
                "{}{}{}{}{}{}{}",
                cell_no_color(self.board[0][y]),
                cell_no_color(self.board[1][y]),
                cell_no_color(self.board[2][y]),
                cell_no_color(self.board[3][y]),
                cell_no_color(self.board[4][y]),
                cell_no_color(self.board[5][y]),
                cell_no_color(self.board[6][y]),
            )?
        }
        writeln!(f, " 0 1 2 3 4 5 6")?;
        Ok(f)
    }
}

fn cell(c: Option<Player>) -> &'static str {
    match c {
        Some(c) => match c {
            Player::Red => "\x1b[94m\x1b[7m\x1b[41m\u{e0b6}\u{e0b4}\x1b[0m",
            Player::Yellow => "\x1b[94m\x1b[7m\x1b[103m\u{e0b6}\u{e0b4}\x1b[0m",
        },

        None => "\x1b[7m\x1b[94m\u{e0b6}\u{e0b4}\x1b[0m",
    }
}
fn cell_no_color(c: Option<Player>) -> &'static str {
    match c {
        Some(c) => match c {
            Player::Red => "\u{e0b6}\u{e0b4}",
            Player::Yellow => "\u{e0b7}\u{e0b5}",
        },

        None => "[]",
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub struct Move(u8);

impl monte_carlo::Move<Board, Player> for Move {
    fn play(&self, board: &mut Board, turn: Player) {
        let y_slice = &mut board.board[usize::from(self.0)];
        for row in y_slice {
            if row.is_none() {
                *row = Some(turn);
                break;
            }
        }
    }

    fn unplay(&self, board: &mut Board, turn: Player) {
        unimplemented!()
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum Player {
    Red,
    Yellow,
}

impl monte_carlo::Turn for Player {
    fn next(self) -> Self {
        match self {
            Player::Red => Self::Yellow,
            Player::Yellow => Self::Red,
        }
    }

    fn prev(self) -> Self {
        self.next()
    }
}

impl monte_carlo::Game for FourInARow {
    type Move = Move;
    type Board = Board;
    type Turn = Player;
}

fn main() {
    let mut they_start = uquery::boolean("Would you like to play first? ", Some(true)).unwrap();
    let player = if they_start {
        Player::Yellow
    } else {
        Player::Red
    };
    let mut state = State::<FourInARow>::new(Player::Red, player, Board::default());
    loop {
        if they_start {
            they_start = false;
        } else {
            state.mcts(12000);
            // write_tree_to_dot(&state);
            let mv = state.suggest_move(MOVE_POLICY);
            match mv {
                Some(mv) => {
                    state.play_move(mv);
                }
                None => panic!("I don't know how to play!"),
            }
        }
        let root = state.root;
        let (board, _) = state.build_board(root);
        println!("--- Them");
        println!("{}", board);
        if let Some(completion) = board.completion_state() {
            match completion {
                Terminal::Won(completion) => match completion.turn {
                    Player::Red => println!("Red wins!"),
                    Player::Yellow => println!("Yellow wins"),
                },
                Terminal::Drawn => println!("Draw"),
            }
            break;
        }
        let mv: u8 = uquery::parsable("Enter move (0-6): ").unwrap();
        state.play_move(Move(mv));
        let root = state.root;
        let (board, _) = state.build_board(root);
        println!("--- You");
        println!("{}", board);
        if let Some(completion) = board.completion_state() {
            match completion {
                Terminal::Won(completion) => match completion.turn {
                    Player::Red => println!("Red wins!"),
                    Player::Yellow => println!("Yellow wins"),
                },
                Terminal::Drawn => println!("Draw"),
            }
            break;
        }
    }
}

fn write_tree_to_dot(state: &State<FourInARow>) {
    let mut file = File::create("file.dot").unwrap();
    writeln!(
        file,
        "{}",
        TreeWrapper::new(&state.tree, state.root, |x| format!(
            "{}\nid: {:?}\nmove: {:?}\nplayouts: {}\nscore: {}\nis_complete: {}",
            state.build_board(x.id()).0.fmt_no_color().unwrap(),
            x.id(),
            x.value().r#move,
            x.value().playouts,
            x.value().score,
            x.value().is_complete,
        ))
    )
    .unwrap();
}
